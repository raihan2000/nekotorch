# Simple torch extension
> For now **only** works with **sdm845** based devices.
## Installation guide
1. Open ``/etc/udev/rules.d/99-flash.rules`` in your faivourite edior
2.  Paste this 
    >``SUBSYSTEM=="leds", DEVPATH=="*/*:flash", RUN+="/bin/chgrp -R torch /sys%p", RUN+="/bin/chmod -R g=u /sys%p"
    ``
3. Add you into torch group
    >``sudo usermod -a -G torch $USER``
4. Clone repo into extensions folder
    >``git clone https://gitlab.com/NekoCWD/nekotorch.git ~/.local/share/gnome-shell/extensions/nekotorch@nekocwd.gitlab.com``
5. Reboot
6. Enable extension via ``gnome-extensions-app``
### If you see this in master branch and last commit older than 2 days, ping me on matrix ``@nekocwd:matrix.org``
